package com.epam.view;

import java.util.*;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Scanner input = new Scanner(System.in);

    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1",bundle.getString("1"));
        menu.put("2",bundle.getString("2"));
        menu.put("3",bundle.getString("3"));
        menu.put("Q",bundle.getString("Q"));
    }

    public View() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1",this::internationalizeMenuUkraine);
        methodMenu.put("2",this::internationalizeMenuEnglish);
        methodMenu.put("3",this::internationalizeMenuSpain);
    }

    private void internationalizeMenuUkraine() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        show();
    }
    private void internationalizeMenuSpain() {
        locale = new Locale("sp");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        show();
    }
//----------------------------------------------------------------------------------------------------
    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

}
