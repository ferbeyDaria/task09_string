package com.epam;

import com.epam.view.View;

public class App {

    public static void main(String[] args) {
        new View().show();
    }
}
